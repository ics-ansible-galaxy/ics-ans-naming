import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('naming')


def test_application_container(host):
    with host.sudo():
        app = host.docker("naming")
        assert app.is_running


def test_application_ui(host):
    cmd = host.run("curl --insecure --fail https://ics-ans-naming-default/")
    assert cmd.rc == 0
    assert "<title>Naming Service</title>" in cmd.stdout


def test_application_api(host):
    cmd = host.run("curl --insecure --fail https://ics-ans-naming-default/rest/healthcheck")
    assert cmd.rc == 0


def test_database_container(host):
    with host.sudo():
        database = host.docker("naming-database")
        assert database.is_running
